export const API_URL = 'http://localhost:8080'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
export const USER_API_URL = `${API_URL}/users`
export const PRODUCT_API_URL = `${API_URL}/products`