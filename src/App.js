import React, { Component } from 'react';
import './App.css';
import './bootstrap.css';
import PresentationApp from './component/PresentationApp.jsx';

class App extends Component {

  render() {
    return (
      <div className="container">
        <PresentationApp />
      </div>
    );
  }
}

export default App;