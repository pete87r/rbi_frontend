import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import AuthenticationService from '../service/AuthenticationService.js'


class HeaderComponent extends Component {
    render() {
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <ul className="navbar-nav">
                        <Link className="nav-link" to="/users">Users</Link>
                        <Link className="nav-link" to="/products">Product</Link>
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        <Link className="nav-link" to="/login" onClick={AuthenticationService.logout}>Logout</Link>
                    </ul>
                </nav>
            </header>
        )
    }
}

export default HeaderComponent