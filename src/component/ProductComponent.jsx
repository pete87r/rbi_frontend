import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import ProductDataService from '../service/ProductDataService';

class ProductComponent extends Component {

  constructor(props) {
    super(props)

    this.state = {
        id: this.props.match.params.id,
        name: '',
        color: ''
    }

    this.onSubmit = this.onSubmit.bind(this)
    this.validate = this.validate.bind(this)
}

componentDidMount() {

    console.log(this.state.id)

    if (this.state.id === '-1') {
      return
    }

    ProductDataService.retrieveProduct(this.state.id)
        .then(response => this.setState({
            name: response.data.name,
            color: response.data.color
        }))
}

onSubmit(values) {

  let product = {
    id: this.state.id,
    name: values.name,
    color: values.color
  }

  if (this.state.id === '-1') {
    ProductDataService.createProduct(product)
        .then(() => this.props.history.push(`/products`))
  } else {
    ProductDataService.updateProduct(this.state.id, product)
        .then(() => this.props.history.push(`/products`))
  }
  
  console.log(values);
}

validate(values) {
  let errors = {}
  if (!values.name) {
      errors.neme = 'Enter a name'
  } 

  return errors
}

render() {

    let { name, color, id } = this.state

return (
        <div>
            <h3>Product</h3>
            <div className="container">
              <Formik
                    initialValues={{ name, color, id }}
                    onSubmit={this.onSubmit}
                    validateOnChange={false}
                    validateOnBlur={false}
                    validate={this.validate}
                    enableReinitialize={true}
                >
                    {
                        (props) => (
                            <Form>
                              <ErrorMessage name="description" component="div" className="alert alert-warning" />
                                <fieldset className="form-group">
                                    <label>Name</label>
                                    <Field className="form-control" type="text" name="name" />
                                </fieldset>
                                <fieldset className="form-group">
                                    <label>Color</label>
                                    <Field className="form-control" type="text" name="color" />
                                </fieldset>
                                <button className="btn btn-success" type="submit">Save</button>
                            </Form>
                        )
                    }
                </Formik>
            </div>
        </div>
    )
}

}

export default ProductComponent