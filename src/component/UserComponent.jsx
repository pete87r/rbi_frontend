import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import UserDataService from '../service/UserDataService';

class UserComponent extends Component {

  constructor(props) {
    super(props)

    this.state = {
        id: this.props.match.params.id,
        name: '',
        adress: ''
    }

    this.onSubmit = this.onSubmit.bind(this)
    this.validate = this.validate.bind(this)
}

componentDidMount() {

    console.log(this.state.id)

    if (this.state.id === '-1') {
      return
    }

    UserDataService.retrieveUser(this.state.id)
        .then(response => this.setState({
            name: response.data.name,
            address: response.data.address
        }))
}

onSubmit(values) {

  let user = {
    id: this.state.id,
    name: values.name,
    address: values.address
  }

  if (this.state.id === '-1') {
    UserDataService.createUser(user)
        .then(() => this.props.history.push(`/users`))
  } else {
    UserDataService.updateUser(this.state.id, user)
        .then(() => this.props.history.push(`/users`))
  }
  
  console.log(values);
}

validate(values) {
  let errors = {}
  if (!values.name) {
      errors.neme = 'Enter a name'
  } 

  return errors
}

render() {

    let { name, address, id } = this.state

return (
        <div>
            <h3>User</h3>
            <div className="container">
              <Formik
                    initialValues={{ name, address, id}}
                    onSubmit={this.onSubmit}
                    validateOnChange={false}
                    validateOnBlur={false}
                    validate={this.validate}
                    enableReinitialize={true}
                >
                    {
                        (props) => (
                            <Form>
                              <ErrorMessage name="description" component="div" className="alert alert-warning" />
                                <fieldset className="form-group">
                                    <label>Name</label>
                                    <Field className="form-control" type="text" name="name" />
                                </fieldset>
                                <fieldset className="form-group">
                                    <label>Address</label>
                                    <Field className="form-control" type="text" name="address" />
                                </fieldset>
                                <button className="btn btn-success" type="submit">Save</button>
                            </Form>
                        )
                    }
                </Formik>
            </div>
        </div>
    )
}
}

export default UserComponent