import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import AuthenticatedRoute from './AuthenticatedRoute.jsx'
import LoginComponent from './LoginComponent.jsx'
import ErrorComponent from './ErrorComponent.jsx'
import HeaderComponent from './HeaderComponent.jsx'
import ProductComponent from './ProductComponent.jsx';
import ProductsListComponent from './ProductsListComponent.jsx';
import UserComponent from './UserComponent.jsx';
import UsersListComponent from './UsersListComponent.jsx';

class TestApp extends Component {
    render() {
        return (
            <div className="TodoApp">
                <Router>
                    <>
                        <HeaderComponent/>
                        <Switch>
                            <Route path="/" exact component={LoginComponent}/>
                            <Route path="/login" component={LoginComponent}/>
                            <AuthenticatedRoute path="/users/:id" component={UserComponent} />
                            <AuthenticatedRoute path="/users" component={UsersListComponent} />
                            <AuthenticatedRoute path="/products/:id" component={ProductComponent} />
                            <AuthenticatedRoute path="/products" component={ProductsListComponent}/>

                            <Route component={ErrorComponent}/>
                        </Switch>
                    </>
                </Router>
            </div>
        )
    }
}

export default TestApp