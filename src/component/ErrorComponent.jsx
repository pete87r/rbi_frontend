import React from 'react'

function ErrorComponent() {
    return <div>Page doesn't exist!</div>
}

export default ErrorComponent