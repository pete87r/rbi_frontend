import axios from 'axios'
import { PRODUCT_API_URL } from '../Constants'

class ProductDataService {

    retrieveAllproducts() {
        return axios.get(`${PRODUCT_API_URL}`);
    }

    retrieveProduct(id) {
        return axios.get(`${PRODUCT_API_URL}/${id}`);
    }

    deleteProduct(id) {
        return axios.delete(`${PRODUCT_API_URL}/${id}`);
    }

    updateProduct(id, product) {
        return axios.put(`${PRODUCT_API_URL}/${id}`, product);
    }
  
    createProduct(product) {
        return axios.post(`${PRODUCT_API_URL}`, product);
    }
}

export default new ProductDataService()
