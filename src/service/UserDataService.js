import axios from 'axios'
import { USER_API_URL } from '../Constants'

class UserDataService {

    retrieveAllUsers() {
        return axios.get(`${USER_API_URL}`);
    }

    retrieveUser(id) {
        return axios.get(`${USER_API_URL}/${id}`);
    }

    deleteUser(id) {
        return axios.delete(`${USER_API_URL}/${id}`);
    }

    updateUser(id, product) {
        return axios.put(`${USER_API_URL}/${id}`, product);
    }
  
    createUser(product) {
        return axios.post(`${USER_API_URL}`, product);
    }
}

export default new UserDataService()
